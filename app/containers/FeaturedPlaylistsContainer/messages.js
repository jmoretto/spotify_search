/*
 * FeaturedPlaylistsContainer Messages
 *
 * This contains all the text for the FeaturedPlaylistsContainer container.
 */

import { defineMessages } from 'react-intl';

export const scope = 'app.containers.FeaturedPlaylistsContainer';

export default defineMessages({
  noResults: {
    id: `${scope}.noResults`,
    defaultMessage: 'No results, try searching for something else!',
  },
  noFilters: {
    id: `${scope}.noFilters`,
    defaultMessage: 'No filters found. Try refreshing the page.',
  },
  filters: {
    id: `${scope}.filters`,
    defaultMessage: 'Filters',
  },
});
