/*
 *
 * FeaturedPlaylistsContainer reducer
 *
 */

import produce from 'immer';
import {
  FETCH_PLAYLISTS,
  FETCH_PLAYLISTS_SUCCESS,
  FETCH_PLAYLISTS_FAILURE,
  FETCH_FILTERS,
  FETCH_FILTERS_SUCCESS,
  FETCH_FILTERS_FAILURE,
} from './constants';

export const initialState = {
  // The structure for the resource for featuredPlaylists state will be:
  // featuredPlaylists: {
  //  resource: { message: "", playlists: [] },
  // }
  featuredPlaylists: {
    resource: {},
    isLoading: false,
    error: null,
  },
  filtersList: {
    resource: {},
    isLoading: false,
    error: null,
  },
};

/* eslint-disable default-case, no-param-reassign */
const featuredPlaylistsContainerReducer = (state = initialState, action) =>
  produce(state, draft => {
    switch (action.type) {
      case FETCH_PLAYLISTS:
        draft.featuredPlaylists = {
          resource: {},
          isLoading: true,
          error: null,
        };
        break;
      case FETCH_PLAYLISTS_SUCCESS:
        draft.featuredPlaylists.resource = action.response;
        draft.featuredPlaylists.isLoading = false;
        break;
      case FETCH_PLAYLISTS_FAILURE:
        draft.featuredPlaylists.isLoading = false;
        draft.featuredPlaylists.error = action.error;
        break;
      case FETCH_FILTERS:
        draft.filtersList = {
          resource: {},
          isLoading: true,
          error: null,
        };
        break;
      case FETCH_FILTERS_SUCCESS:
        draft.filtersList.resource = action.response;
        draft.filtersList.isLoading = false;
        break;
      case FETCH_FILTERS_FAILURE:
        draft.filtersList.isLoading = false;
        draft.filtersList.error = action.error;
        break;
      default:
        break;
    }
  });

export default featuredPlaylistsContainerReducer;
