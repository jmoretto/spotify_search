import {
  fetchPlaylists,
  fetchPlaylistsSuccess,
  fetchPlaylistsFailure,
} from '../actions';
import {
  FETCH_PLAYLISTS,
  FETCH_PLAYLISTS_SUCCESS,
  FETCH_PLAYLISTS_FAILURE,
} from '../constants';

describe('FeaturedPlaylistsContainer actions', () => {
  describe('Playlists actions', () => {
    it('has a type of FETCH_PLAYLISTS', () => {
      const expected = {
        type: FETCH_PLAYLISTS,
        options: {},
      };
      expect(fetchPlaylists()).toEqual(expected);
    });

    it('has a type of FETCH_PLAYLISTS_SUCCESS', () => {
      const expected = {
        type: FETCH_PLAYLISTS_SUCCESS,
        response: 'response',
      };
      expect(fetchPlaylistsSuccess('response')).toEqual(expected);
    });

    it('has a type of FETCH_PLAYLISTS_FAILURE', () => {
      const expected = {
        type: FETCH_PLAYLISTS_FAILURE,
        error: 'error',
      };
      expect(fetchPlaylistsFailure('error')).toEqual(expected);
    });
  });
});
