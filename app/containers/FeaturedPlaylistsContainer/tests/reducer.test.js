import produce from 'immer';
import featuredPlaylistsContainerReducer, { initialState } from '../reducer';
import {
  fetchPlaylists,
  fetchPlaylistsSuccess,
  fetchPlaylistsFailure,
} from '../actions';

/* eslint-disable default-case, no-param-reassign */
describe('featuredPlaylistsContainerReducer', () => {
  let state;
  beforeEach(() => {
    state = {
      ...initialState,
    };
  });

  it('returns the initial state', () => {
    const expectedResult = state;
    expect(featuredPlaylistsContainerReducer(undefined, {})).toEqual(
      expectedResult,
    );
  });

  it('should handle the fetchPlaylists action correctly', () => {
    const expectedResult = produce(state, draft => {
      draft.featuredPlaylists = {
        resource: {},
        isLoading: true,
        error: null,
      };
    });

    expect(featuredPlaylistsContainerReducer(state, fetchPlaylists())).toEqual(
      expectedResult,
    );
  });

  it('should handle the fetchPlaylistsSuccess action correctly', () => {
    const expectedResult = produce(state, draft => {
      draft.featuredPlaylists = {
        resource: 'test',
        isLoading: false,
        error: null,
      };
    });

    expect(
      featuredPlaylistsContainerReducer(state, fetchPlaylistsSuccess('test')),
    ).toEqual(expectedResult);
  });

  it('should handle the fetchPlaylistsFailure action correctly', () => {
    const expectedResult = produce(state, draft => {
      draft.featuredPlaylists = {
        resource: {},
        isLoading: false,
        error: 'error',
      };
    });

    expect(
      featuredPlaylistsContainerReducer(state, fetchPlaylistsFailure('error')),
    ).toEqual(expectedResult);
  });
});
