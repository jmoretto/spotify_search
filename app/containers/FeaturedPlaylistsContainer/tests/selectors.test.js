import {
  selectFeaturedPlaylistsContainer,
  makeSelectFeaturedPlaylists,
} from '../selectors';
import { initialState } from '../reducer';

describe('selectFeaturedPlaylistsContainerDomain', () => {
  it('should correctly select the featured playlists container domain', () => {
    const state = {
      featuredPlaylistsContainer: {},
    };

    expect(selectFeaturedPlaylistsContainer(state)).toEqual({});
  });

  it('should correctly select the featured playlists container domain', () => {
    expect(selectFeaturedPlaylistsContainer({})).toEqual(initialState);
  });

  it('should correctly select the featured playlists', () => {
    const state = {
      featuredPlaylistsContainer: {
        featuredPlaylists: {},
      },
    };
    const selectFeaturedPlaylists = makeSelectFeaturedPlaylists();

    expect(selectFeaturedPlaylists(state)).toEqual({});
    expect(selectFeaturedPlaylists(state)).toEqual({});
    expect(selectFeaturedPlaylists.recomputations()).toEqual(1);
  });
});
