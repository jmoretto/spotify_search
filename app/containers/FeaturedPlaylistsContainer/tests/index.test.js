/**
 *
 * Tests for FeaturedPlaylistsContainer
 *
 * @see https://github.com/react-boilerplate/react-boilerplate/tree/master/docs/testing
 *
 */

import React from 'react';
import { render } from 'react-testing-library';
import { IntlProvider } from 'react-intl';
// import 'jest-dom/extend-expect'; // add some helpful assertions
import { browserHistory } from 'react-router-dom';
import { Provider } from 'react-redux';
import configureStore from '../../../configureStore';

import { FeaturedPlaylistsContainer } from '../index';
import { DEFAULT_LOCALE } from '../../../i18n';

describe('<FeaturedPlaylistsContainer />', () => {
  const props = {
    featuredPlaylists: [
      {
        collaborative: false,
        description:
          'A mega mix of 75 of your favorite songs from the last few years! Cover: Post Malone',
        external_urls: {
          spotify: 'https://open.spotify.com/playlist/37i9dQZF1DXbYM3nMM0oPk',
        },
        href: 'https://api.spotify.com/v1/playlists/37i9dQZF1DXbYM3nMM0oPk',
        id: '37i9dQZF1DXbYM3nMM0oPk',
        images: [
          {
            height: null,
            url:
              'https://i.scdn.co/image/ab67706f00000002002b3728f0d5c5c599b9b06a',
            width: null,
          },
        ],
        name: 'Mega Hit Mix',
        owner: {
          display_name: 'Spotify',
          external_urls: {
            spotify: 'https://open.spotify.com/user/spotify',
          },
          href: 'https://api.spotify.com/v1/users/spotify',
          id: 'spotify',
          type: 'user',
          uri: 'spotify:user:spotify',
        },
        primary_color: null,
        public: null,
        snapshot_id:
          'MTU3NTIzMjU0NSwwMDAwMDAwMGQ0MWQ4Y2Q5OGYwMGIyMDRlOTgwMDk5OGVjZjg0Mjdl',
        tracks: {
          href:
            'https://api.spotify.com/v1/playlists/37i9dQZF1DXbYM3nMM0oPk/tracks',
          total: 75,
        },
        type: 'playlist',
        uri: 'spotify:playlist:37i9dQZF1DXbYM3nMM0oPk',
      },
      {
        collaborative: false,
        description:
          "Because a quiet Sunday every now and again doesn't hurt anybody.",
        external_urls: {
          spotify: 'https://open.spotify.com/playlist/37i9dQZF1DWTc452KG008d',
        },
        href: 'https://api.spotify.com/v1/playlists/37i9dQZF1DWTc452KG008d',
        id: '37i9dQZF1DWTc452KG008d',
        images: [
          {
            height: null,
            url:
              'https://i.scdn.co/image/ab67706f000000023272a833431602b43a231466',
            width: null,
          },
        ],
        name: 'Lazy Sunday',
        owner: {
          display_name: 'Spotify',
          external_urls: {
            spotify: 'https://open.spotify.com/user/spotify',
          },
          href: 'https://api.spotify.com/v1/users/spotify',
          id: 'spotify',
          type: 'user',
          uri: 'spotify:user:spotify',
        },
        primary_color: null,
        public: null,
        snapshot_id:
          'MTU3NTIzMjU1MywwMDAwMDAwMGQ0MWQ4Y2Q5OGYwMGIyMDRlOTgwMDk5OGVjZjg0Mjdl',
        tracks: {
          href:
            'https://api.spotify.com/v1/playlists/37i9dQZF1DWTc452KG008d/tracks',
          total: 50,
        },
        type: 'playlist',
        uri: 'spotify:playlist:37i9dQZF1DWTc452KG008d',
      },
      {
        collaborative: false,
        description:
          'Relax and indulge with some profoundly beautiful piano pieces.',
        external_urls: {
          spotify: 'https://open.spotify.com/playlist/37i9dQZF1DX4sWSpwq3LiO',
        },
        href: 'https://api.spotify.com/v1/playlists/37i9dQZF1DX4sWSpwq3LiO',
        id: '37i9dQZF1DX4sWSpwq3LiO',
        images: [
          {
            height: null,
            url:
              'https://i.scdn.co/image/ab67706f00000002ca5a7517156021292e5663a6',
            width: null,
          },
        ],
        name: 'Peaceful Piano',
        owner: {
          display_name: 'Spotify',
          external_urls: {
            spotify: 'https://open.spotify.com/user/spotify',
          },
          href: 'https://api.spotify.com/v1/users/spotify',
          id: 'spotify',
          type: 'user',
          uri: 'spotify:user:spotify',
        },
        primary_color: null,
        public: null,
        snapshot_id:
          'MTU3NDc2NjQzMiwwMDAwMDBhYTAwMDAwMTZlYTc2NGQwZmIwMDAwMDE2ZDE1NTk1OTFk',
        tracks: {
          href:
            'https://api.spotify.com/v1/playlists/37i9dQZF1DX4sWSpwq3LiO/tracks',
          total: 264,
        },
        type: 'playlist',
        uri: 'spotify:playlist:37i9dQZF1DX4sWSpwq3LiO',
      },
      {
        collaborative: false,
        description:
          'Peak time bangers and emerging future hits. Cover: NERVO, Skazi',
        external_urls: {
          spotify: 'https://open.spotify.com/playlist/37i9dQZF1DX7ZUug1ANKRP',
        },
        href: 'https://api.spotify.com/v1/playlists/37i9dQZF1DX7ZUug1ANKRP',
        id: '37i9dQZF1DX7ZUug1ANKRP',
        images: [
          {
            height: null,
            url:
              'https://i.scdn.co/image/ab67706f00000002d3e5899aafd0a54001727d3e',
            width: null,
          },
        ],
        name: 'Main Stage',
        owner: {
          display_name: 'Spotify',
          external_urls: {
            spotify: 'https://open.spotify.com/user/spotify',
          },
          href: 'https://api.spotify.com/v1/users/spotify',
          id: 'spotify',
          type: 'user',
          uri: 'spotify:user:spotify',
        },
        primary_color: null,
        public: null,
        snapshot_id:
          'MTU3NDg2OTMyMCwwMDAwMDBmNDAwMDAwMTZlYWQ4NmMzZjUwMDAwMDE2ZWFkNjBlYTlh',
        tracks: {
          href:
            'https://api.spotify.com/v1/playlists/37i9dQZF1DX7ZUug1ANKRP/tracks',
          total: 50,
        },
        type: 'playlist',
        uri: 'spotify:playlist:37i9dQZF1DX7ZUug1ANKRP',
      },
      {
        collaborative: false,
        description:
          'A collection of powerful voices and dark, alluring siren songs. Cover: Caitlyn Smith.',
        external_urls: {
          spotify: 'https://open.spotify.com/playlist/37i9dQZF1DX0IyMQV27EGn',
        },
        href: 'https://api.spotify.com/v1/playlists/37i9dQZF1DX0IyMQV27EGn',
        id: '37i9dQZF1DX0IyMQV27EGn',
        images: [
          {
            height: null,
            url:
              'https://i.scdn.co/image/ab67706f0000000222b9f9ee6d425646a0ec94d5',
            width: null,
          },
        ],
        name: 'Femme Fatale',
        owner: {
          display_name: 'Spotify',
          external_urls: {
            spotify: 'https://open.spotify.com/user/spotify',
          },
          href: 'https://api.spotify.com/v1/users/spotify',
          id: 'spotify',
          type: 'user',
          uri: 'spotify:user:spotify',
        },
        primary_color: null,
        public: null,
        snapshot_id:
          'MTU3NTIzMjU1OCwwMDAwMDAwMGQ0MWQ4Y2Q5OGYwMGIyMDRlOTgwMDk5OGVjZjg0Mjdl',
        tracks: {
          href:
            'https://api.spotify.com/v1/playlists/37i9dQZF1DX0IyMQV27EGn/tracks',
          total: 80,
        },
        type: 'playlist',
        uri: 'spotify:playlist:37i9dQZF1DX0IyMQV27EGn',
      },
      {
        collaborative: false,
        description: 'Relax to the sound of jazz.',
        external_urls: {
          spotify: 'https://open.spotify.com/playlist/37i9dQZF1DWVqfgj8NZEp1',
        },
        href: 'https://api.spotify.com/v1/playlists/37i9dQZF1DWVqfgj8NZEp1',
        id: '37i9dQZF1DWVqfgj8NZEp1',
        images: [
          {
            height: null,
            url:
              'https://i.scdn.co/image/ab67706f000000028df7fedfed909f10628586fe',
            width: null,
          },
        ],
        name: 'Coffee Table Jazz',
        owner: {
          display_name: 'Spotify',
          external_urls: {
            spotify: 'https://open.spotify.com/user/spotify',
          },
          href: 'https://api.spotify.com/v1/users/spotify',
          id: 'spotify',
          type: 'user',
          uri: 'spotify:user:spotify',
        },
        primary_color: null,
        public: null,
        snapshot_id:
          'MTU3MzE4OTI2MCwwMDAwMDA1YTAwMDAwMTZlNDk2MzEzODEwMDAwMDE2ZDAwY2EyYTc2',
        tracks: {
          href:
            'https://api.spotify.com/v1/playlists/37i9dQZF1DWVqfgj8NZEp1/tracks',
          total: 131,
        },
        type: 'playlist',
        uri: 'spotify:playlist:37i9dQZF1DWVqfgj8NZEp1',
      },
      {
        collaborative: false,
        description: 'Stay cozy as the sounds of the evening unfold...',
        external_urls: {
          spotify: 'https://open.spotify.com/playlist/37i9dQZF1DXcWBRiUaG3o5',
        },
        href: 'https://api.spotify.com/v1/playlists/37i9dQZF1DXcWBRiUaG3o5',
        id: '37i9dQZF1DXcWBRiUaG3o5',
        images: [
          {
            height: null,
            url:
              'https://i.scdn.co/image/ab67706f00000002bdd580456b856d8e1176ffb1',
            width: null,
          },
        ],
        name: 'Evening Acoustic',
        owner: {
          display_name: 'Spotify',
          external_urls: {
            spotify: 'https://open.spotify.com/user/spotify',
          },
          href: 'https://api.spotify.com/v1/users/spotify',
          id: 'spotify',
          type: 'user',
          uri: 'spotify:user:spotify',
        },
        primary_color: null,
        public: null,
        snapshot_id:
          'MTU2NzY3NTMwNSwwMDAwMDA0OTAwMDAwMTZjZGEzNDBiMDUwMDAwMDE2ZDAwYmFjZWVh',
        tracks: {
          href:
            'https://api.spotify.com/v1/playlists/37i9dQZF1DXcWBRiUaG3o5/tracks',
          total: 109,
        },
        type: 'playlist',
        uri: 'spotify:playlist:37i9dQZF1DXcWBRiUaG3o5',
      },
      {
        collaborative: false,
        description: 'Hits to boost your mood and fill you with happiness!',
        external_urls: {
          spotify: 'https://open.spotify.com/playlist/37i9dQZF1DXdPec7aLTmlC',
        },
        href: 'https://api.spotify.com/v1/playlists/37i9dQZF1DXdPec7aLTmlC',
        id: '37i9dQZF1DXdPec7aLTmlC',
        images: [
          {
            height: null,
            url:
              'https://i.scdn.co/image/ab67706f000000025af1070c80cd50dbbb4cfa19',
            width: null,
          },
        ],
        name: 'Happy Hits!',
        owner: {
          display_name: 'Spotify',
          external_urls: {
            spotify: 'https://open.spotify.com/user/spotify',
          },
          href: 'https://api.spotify.com/v1/users/spotify',
          id: 'spotify',
          type: 'user',
          uri: 'spotify:user:spotify',
        },
        primary_color: null,
        public: null,
        snapshot_id:
          'MTU3NTIzMjUwOCwwMDAwMDAwMGQ0MWQ4Y2Q5OGYwMGIyMDRlOTgwMDk5OGVjZjg0Mjdl',
        tracks: {
          href:
            'https://api.spotify.com/v1/playlists/37i9dQZF1DXdPec7aLTmlC/tracks',
          total: 100,
        },
        type: 'playlist',
        uri: 'spotify:playlist:37i9dQZF1DXdPec7aLTmlC',
      },
      {
        collaborative: false,
        description:
          'A selection of the greatest funk & soul records ever made.',
        external_urls: {
          spotify: 'https://open.spotify.com/playlist/37i9dQZF1DWWvhKV4FBciw',
        },
        href: 'https://api.spotify.com/v1/playlists/37i9dQZF1DWWvhKV4FBciw',
        id: '37i9dQZF1DWWvhKV4FBciw',
        images: [
          {
            height: null,
            url:
              'https://i.scdn.co/image/ab67706f00000002c98a8e76cd6a249c74a46431',
            width: null,
          },
        ],
        name: 'Funk & Soul Classics',
        owner: {
          display_name: 'Spotify',
          external_urls: {
            spotify: 'https://open.spotify.com/user/spotify',
          },
          href: 'https://api.spotify.com/v1/users/spotify',
          id: 'spotify',
          type: 'user',
          uri: 'spotify:user:spotify',
        },
        primary_color: null,
        public: null,
        snapshot_id:
          'MTU3NTIzMjUxOCwwMDAwMDAwMGQ0MWQ4Y2Q5OGYwMGIyMDRlOTgwMDk5OGVjZjg0Mjdl',
        tracks: {
          href:
            'https://api.spotify.com/v1/playlists/37i9dQZF1DWWvhKV4FBciw/tracks',
          total: 80,
        },
        type: 'playlist',
        uri: 'spotify:playlist:37i9dQZF1DWWvhKV4FBciw',
      },
      {
        collaborative: false,
        description:
          'From disco to soft rock, and funk to punk, the ‘70s had a little something for everyone.',
        external_urls: {
          spotify: 'https://open.spotify.com/playlist/37i9dQZF1DWTJ7xPn4vNaz',
        },
        href: 'https://api.spotify.com/v1/playlists/37i9dQZF1DWTJ7xPn4vNaz',
        id: '37i9dQZF1DWTJ7xPn4vNaz',
        images: [
          {
            height: null,
            url:
              'https://i.scdn.co/image/ab67706f000000023959628a615235b109e3ca56',
            width: null,
          },
        ],
        name: 'All Out 70s',
        owner: {
          display_name: 'Spotify',
          external_urls: {
            spotify: 'https://open.spotify.com/user/spotify',
          },
          href: 'https://api.spotify.com/v1/users/spotify',
          id: 'spotify',
          type: 'user',
          uri: 'spotify:user:spotify',
        },
        primary_color: null,
        public: null,
        snapshot_id:
          'MTU3NTIzMjU2OCwwMDAwMDAwMGQ0MWQ4Y2Q5OGYwMGIyMDRlOTgwMDk5OGVjZjg0Mjdl',
        tracks: {
          href:
            'https://api.spotify.com/v1/playlists/37i9dQZF1DWTJ7xPn4vNaz/tracks',
          total: 104,
        },
        type: 'playlist',
        uri: 'spotify:playlist:37i9dQZF1DWTJ7xPn4vNaz',
      },
      {
        collaborative: false,
        description: "Lofi's crispiest beatmakers. Cover: Cookin Soul",
        external_urls: {
          spotify: 'https://open.spotify.com/playlist/37i9dQZF1DX36Xw4IJIVKA',
        },
        href: 'https://api.spotify.com/v1/playlists/37i9dQZF1DX36Xw4IJIVKA',
        id: '37i9dQZF1DX36Xw4IJIVKA',
        images: [
          {
            height: null,
            url:
              'https://pl.scdn.co/images/pl/default/493aa677d1976bdb87f467fe7b78dd2df4937934',
            width: null,
          },
        ],
        name: 'Lofi Hip-Hop',
        owner: {
          display_name: 'Spotify',
          external_urls: {
            spotify: 'https://open.spotify.com/user/spotify',
          },
          href: 'https://api.spotify.com/v1/users/spotify',
          id: 'spotify',
          type: 'user',
          uri: 'spotify:user:spotify',
        },
        primary_color: null,
        public: null,
        snapshot_id:
          'MTU3NDQ0NTM2NywwMDAwMDA0NTAwMDAwMTZlOTQ0MWMwOTQwMDAwMDE2ODk5NzQ1YWVi',
        tracks: {
          href:
            'https://api.spotify.com/v1/playlists/37i9dQZF1DX36Xw4IJIVKA/tracks',
          total: 99,
        },
        type: 'playlist',
        uri: 'spotify:playlist:37i9dQZF1DX36Xw4IJIVKA',
      },
      {
        collaborative: false,
        description:
          'Slow, sludgy stoner rock. You can practically taste the desert dirt in your mouth while listening to these dark blues and rough rock numbers.',
        external_urls: {
          spotify: 'https://open.spotify.com/playlist/37i9dQZF1DXdpVGstUksUC',
        },
        href: 'https://api.spotify.com/v1/playlists/37i9dQZF1DXdpVGstUksUC',
        id: '37i9dQZF1DXdpVGstUksUC',
        images: [
          {
            height: null,
            url:
              'https://pl.scdn.co/images/pl/default/dd2559533d97c897d29365dc80d3eec30676786a',
            width: null,
          },
        ],
        name: 'Stoner Rock',
        owner: {
          display_name: 'Spotify',
          external_urls: {
            spotify: 'https://open.spotify.com/user/spotify',
          },
          href: 'https://api.spotify.com/v1/users/spotify',
          id: 'spotify',
          type: 'user',
          uri: 'spotify:user:spotify',
        },
        primary_color: null,
        public: null,
        snapshot_id:
          'MTU3NTIzMjU2MSwwMDAwMDAwMGQ0MWQ4Y2Q5OGYwMGIyMDRlOTgwMDk5OGVjZjg0Mjdl',
        tracks: {
          href:
            'https://api.spotify.com/v1/playlists/37i9dQZF1DXdpVGstUksUC/tracks',
          total: 54,
        },
        type: 'playlist',
        uri: 'spotify:playlist:37i9dQZF1DXdpVGstUksUC',
      },
      {
        collaborative: false,
        description: 'Feel good with this positively timeless playlist!',
        external_urls: {
          spotify: 'https://open.spotify.com/playlist/37i9dQZF1DX9XIFQuFvzM4',
        },
        href: 'https://api.spotify.com/v1/playlists/37i9dQZF1DX9XIFQuFvzM4',
        id: '37i9dQZF1DX9XIFQuFvzM4',
        images: [
          {
            height: null,
            url:
              'https://i.scdn.co/image/ab67706f0000000213a02d059c0479e65a850267',
            width: null,
          },
        ],
        name: "Feelin' Good",
        owner: {
          display_name: 'Spotify',
          external_urls: {
            spotify: 'https://open.spotify.com/user/spotify',
          },
          href: 'https://api.spotify.com/v1/users/spotify',
          id: 'spotify',
          type: 'user',
          uri: 'spotify:user:spotify',
        },
        primary_color: null,
        public: null,
        snapshot_id:
          'MTU3NTIzMjU2NCwwMDAwMDAwMGQ0MWQ4Y2Q5OGYwMGIyMDRlOTgwMDk5OGVjZjg0Mjdl',
        tracks: {
          href:
            'https://api.spotify.com/v1/playlists/37i9dQZF1DX9XIFQuFvzM4/tracks',
          total: 100,
        },
        type: 'playlist',
        uri: 'spotify:playlist:37i9dQZF1DX9XIFQuFvzM4',
      },
    ],
    filtersList: [
      {
        id: 'locale',
        name: 'Locale',
        values: [
          {
            value: 'en_AU',
            name: 'en_AU',
          },
          {
            value: 'de_DE',
            name: 'de_DE ',
          },
          {
            value: 'pt_BR',
            name: 'pt_BR',
          },
          {
            value: 'fr_FR',
            name: 'fr_FR',
          },
          {
            value: 'en_US',
            name: 'en_US',
          },
          {
            value: 'es_AR',
            name: 'es_AR',
          },
        ],
      },
      {
        id: 'country',
        name: 'País',
        values: [
          {
            value: 'AU',
            name: 'Australia',
          },
          {
            value: 'DE',
            name: 'Alemanhã',
          },
          {
            value: 'BR',
            name: 'Brasil',
          },
          {
            value: 'PT',
            name: 'Portugal',
          },
          {
            value: 'en_US',
            name: 'EUA',
          },
          {
            value: 'RU',
            name: 'Rússia',
          },
        ],
      },
      {
        id: 'timestamp',
        name: 'Data e Horário',
        validation: {
          primitiveType: 'STRING',
          entityType: 'DATE_TIME',
          pattern: 'yyyy-MM-ddTHH:mm:ss',
        },
      },
      {
        id: 'limit',
        name: 'Quantidade',
        validation: {
          primitiveType: 'INTEGER',
          min: 1,
          max: 50,
        },
      },
      {
        id: 'offset',
        name: 'Página',
        validation: {
          primitiveType: 'INTEGER',
        },
      },
    ],
    featuredPlaylistsIsLoading: false,
    featuredPlayListsError: null,
    featuredPlaylistsMessage: 'messages',
    filtersListIsLoading: false,
    filtersListError: null,
    fetchPlaylists: () => {},
    fetchPlaylistsCancel: () => {},
    verifyToken: () => {},
    location: {},
  };

  let store;
  beforeAll(() => {
    store = configureStore({}, browserHistory);
  });

  it('Expect to not log errors in console', () => {
    const spy = jest.spyOn(global.console, 'error');
    const dispatch = jest.fn();
    render(
      <Provider store={store}>
        <IntlProvider locale={DEFAULT_LOCALE}>
          <FeaturedPlaylistsContainer dispatch={dispatch} {...props} />
        </IntlProvider>
      </Provider>,
    );
    expect(spy).not.toHaveBeenCalled();
  });

  it('Should render and match the snapshot', () => {
    const {
      container: { firstChild },
    } = render(
      <Provider store={store}>
        <IntlProvider locale={DEFAULT_LOCALE}>
          <FeaturedPlaylistsContainer {...props} />
        </IntlProvider>
      </Provider>,
    );
    expect(firstChild).toMatchSnapshot();
  });
});
