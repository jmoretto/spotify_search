/*
 *
 * FeaturedPlaylistsContainer actions
 *
 */

import {
  FETCH_PLAYLISTS,
  FETCH_PLAYLISTS_FAILURE,
  FETCH_PLAYLISTS_SUCCESS,
  FETCH_PLAYLISTS_CANCEL,
  FETCH_FILTERS,
  FETCH_FILTERS_FAILURE,
  FETCH_FILTERS_SUCCESS,
  VERIFY_TOKEN,
} from './constants';

export function fetchPlaylists(options = {}) {
  return {
    type: FETCH_PLAYLISTS,
    options,
  };
}

export function fetchPlaylistsSuccess(response) {
  return {
    type: FETCH_PLAYLISTS_SUCCESS,
    response,
  };
}

export function fetchPlaylistsFailure(error) {
  return {
    type: FETCH_PLAYLISTS_FAILURE,
    error,
  };
}

export function fetchPlaylistsCancel() {
  return {
    type: FETCH_PLAYLISTS_CANCEL,
  };
}

export function fetchFilters() {
  return {
    type: FETCH_FILTERS,
  };
}

export function fetchFiltersSuccess(response) {
  return {
    type: FETCH_FILTERS_SUCCESS,
    response,
  };
}

export function fetchFiltersFailure(error) {
  return {
    type: FETCH_FILTERS_FAILURE,
    error,
  };
}

export function verifyToken(hash) {
  return {
    type: VERIFY_TOKEN,
    hash,
  };
}
