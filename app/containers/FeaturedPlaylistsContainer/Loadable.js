/**
 *
 * Asynchronously loads the component for FeaturedPlaylistsContainer
 *
 */

import loadable from 'utils/loadable';

export default loadable(() => import('./index'));
