/**
 * Sagas related to the FeaturedPlaylistsContainer
 */

import {
  call,
  put,
  takeLatest,
  delay,
  fork,
  take,
  cancel,
} from 'redux-saga/effects';

import request from 'utils/request';
import * as qs from 'query-string';
import history from 'utils/history';
import { FETCH_FILTERS_API, SPOTIFY_FEATURED_PLAYLISTS_API } from 'urls';
import {
  fetchFiltersSuccess,
  fetchFiltersFailure,
  fetchFilters,
  fetchPlaylistsSuccess,
  fetchPlaylistsFailure,
  fetchPlaylists,
} from './actions';
import {
  FETCH_FILTERS,
  VERIFY_TOKEN,
  FETCH_PLAYLISTS,
  FETCH_PLAYLISTS_CANCEL,
} from './constants';

/**
 * Verify if the user has authenticated with spotify and has a token ready in the URL
 * If not then we should redirect the user back to the authentication page (/).
 * If he has then we should fetch the filter list.
 */
export function* verifyToken(action) {
  const query = qs.parse(action.hash);
  if (action.hash && query.access_token) {
    // We have an access token, store in local storage and fetch
    // the filter and playlists.
    localStorage.setItem('token', query.access_token);
    yield put(fetchFilters());
    yield put(fetchPlaylists());
  } else if (!localStorage.getItem('token')) {
    // We don't have an access token redirect the user to the
    // root page so that he can re-authorize.
    history.push('/');
  } else {
    yield put(fetchFilters());
    yield put(fetchPlaylists());
  }
}

/**
 * Get filters request/response handler
 */
export function* getFilters() {
  try {
    const response = yield call(request, FETCH_FILTERS_API);
    yield put(fetchFiltersSuccess(response));
  } catch (err) {
    yield put(fetchFiltersFailure(err));
  }
}

/**
 * Get playlists request/response handler
 */
export function* getPlaylists(action) {
  const options = {
    method: 'get',
    headers: new Headers({
      Authorization: `Bearer ${localStorage.getItem('token')}`,
    }),
  };

  // Check if there are parameters we need to pass as query string.
  const searchParams = new URLSearchParams(action.options);
  const url = new URL(SPOTIFY_FEATURED_PLAYLISTS_API);
  url.search = searchParams.toString();

  while (true) {
    try {
      const response = yield call(request, url, options);
      yield put(fetchPlaylistsSuccess(response));
    } catch (err) {
      yield put(fetchPlaylistsFailure(err));
    }

    // Repeat this request in 30 seconds to check if anything changed.
    yield delay(30000);
  }
}

function* startFetchPlaylists(action) {
  // starts the task in the background
  const fetchPlaylistsTask = yield fork(getPlaylists, action);

  // wait for the stop action
  yield take(FETCH_PLAYLISTS_CANCEL);

  // this will cause the forked fetchPlaylistsTask task to be cancelled.
  yield cancel(fetchPlaylistsTask);
}

/**
 * Root saga manages watcher lifecycle
 */
export default function* featuredPlaylistsListener() {
  yield takeLatest(VERIFY_TOKEN, verifyToken);
  yield takeLatest(FETCH_FILTERS, getFilters);
  yield takeLatest(FETCH_PLAYLISTS, startFetchPlaylists);
}
