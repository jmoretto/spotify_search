/**
 *
 * FeaturedPlaylistsContainer
 *
 */

import React, { memo, useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { FormattedMessage } from 'react-intl';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';

import { useInjectReducer } from 'utils/injectReducer';
import { useInjectSaga } from 'utils/injectSaga';
import FeaturedPlaylistList from 'components/FeaturedPlaylistList';
import FiltersList from 'components/FiltersList';
import LoadingIndicator from 'components/LoadingIndicator';
import {
  makeSelectFeaturedPlaylistsIsLoading,
  makeSelectFeaturedPlaylistsError,
  makeSelectFeaturedPlaylistsItems,
  makeSelectFeaturedPlaylistsMessage,
  makeSelectFiltersListList,
  makeSelectFiltersListIsLoading,
  makeSelectFiltersListError,
} from './selectors';
import reducer from './reducer';
import messages from './messages';
import saga from './sagas';
import { verifyToken, fetchPlaylists, fetchPlaylistsCancel } from './actions';

export const FeaturedPlaylistsContainer = ({
  featuredPlaylistsIsLoading,
  featuredPlayListsError,
  featuredPlaylists,
  featuredPlaylistsMessage,
  filtersListIsLoading,
  filtersListError,
  filtersList,
  location,
  ...props
}) => {
  const [options, setOptions] = useState({});
  const [nameFilter, setNameFilter] = useState(null);
  useInjectReducer({ key: 'featuredPlaylistsContainer', reducer });
  useInjectSaga({ key: 'featuredPlaylistsContainer', saga });

  useEffect(() => {
    // Verify if we have a token for fetching playlists
    props.verifyToken(location.hash);
  }, []);

  useEffect(() => {
    // Fetch playlists after every change to the options.
    props.fetchPlaylistsCancel();
    props.fetchPlaylists(options);
  }, [options]);

  const filterPlaylists = () => {
    if (nameFilter) {
      return featuredPlaylists.filter(playlist =>
        playlist.name.toLowerCase().includes(nameFilter.toLowerCase()),
      );
    }

    return featuredPlaylists;
  };

  const renderPlaylists = () => {
    if (!featuredPlaylistsIsLoading) {
      if (!featuredPlayListsError) {
        return featuredPlaylists && featuredPlaylists.length ? (
          <FeaturedPlaylistList playlists={filterPlaylists()} />
        ) : (
          <div className="text-xl">
            <FormattedMessage {...messages.noResults} />
          </div>
        );
      }

      return <h1>{featuredPlayListsError.message}</h1>;
    }

    return <LoadingIndicator />;
  };

  const renderFiltersList = () => {
    if (!filtersListIsLoading) {
      if (!filtersListError) {
        return filtersList && filtersList.length ? (
          <FiltersList
            filters={filtersList}
            values={options}
            onChange={(filterName, filterValue) => {
              if (!filterValue) {
                const newOptions = { ...options };
                delete newOptions[filterName];
                setOptions(newOptions);
              } else {
                setOptions({ ...options, [filterName]: filterValue });
              }
            }}
            onChangeName={(_, filterValue) => {
              setNameFilter(filterValue);
            }}
            nameFilterValue={nameFilter}
          />
        ) : (
          <div className="text-xl">
            <FormattedMessage {...messages.noFilters} />
          </div>
        );
      }

      return <h1>{filtersListError.message}</h1>;
    }

    return <LoadingIndicator />;
  };

  return (
    <div>
      <section>
        <h1>
          <FormattedMessage {...messages.filters} />
        </h1>
        {renderFiltersList()}
      </section>
      <section>
        <h1>{featuredPlaylistsMessage}</h1>
        {renderPlaylists()}
      </section>
    </div>
  );
};

FeaturedPlaylistsContainer.propTypes = {
  featuredPlaylistsIsLoading: PropTypes.bool,
  featuredPlayListsError: PropTypes.object,
  featuredPlaylists: PropTypes.array,
  featuredPlaylistsMessage: PropTypes.string,
  filtersListIsLoading: PropTypes.bool,
  filtersListError: PropTypes.object,
  filtersList: PropTypes.array,
  fetchPlaylists: PropTypes.func,
  fetchPlaylistsCancel: PropTypes.func,
  verifyToken: PropTypes.func,
  location: PropTypes.object,
};

const mapStateToProps = createStructuredSelector({
  featuredPlaylistsIsLoading: makeSelectFeaturedPlaylistsIsLoading(),
  featuredPlayListsError: makeSelectFeaturedPlaylistsError(),
  featuredPlaylists: makeSelectFeaturedPlaylistsItems(),
  featuredPlaylistsMessage: makeSelectFeaturedPlaylistsMessage(),
  filtersList: makeSelectFiltersListList(),
  filtersListIsLoading: makeSelectFiltersListIsLoading(),
  filtersListError: makeSelectFiltersListError(),
});

const withConnect = connect(
  mapStateToProps,
  { verifyToken, fetchPlaylists, fetchPlaylistsCancel },
);

export default compose(
  withConnect,
  memo,
)(FeaturedPlaylistsContainer);
