import { createSelector } from 'reselect';
import get from 'lodash/get';
import { initialState } from './reducer';

const selectFeaturedPlaylistsContainer = state =>
  state.featuredPlaylistsContainer || initialState;

// Featured playlists selectors
const makeSelectFeaturedPlaylists = () =>
  createSelector(
    selectFeaturedPlaylistsContainer,
    substate => substate.featuredPlaylists || {},
  );

const makeSelectFeaturedPlaylistsResource = () =>
  createSelector(
    makeSelectFeaturedPlaylists(),
    substate => substate.resource || {},
  );

const makeSelectFeaturedPlaylistsIsLoading = () =>
  createSelector(
    makeSelectFeaturedPlaylists(),
    substate => substate.isLoading || false,
  );

const makeSelectFeaturedPlaylistsError = () =>
  createSelector(
    makeSelectFeaturedPlaylists(),
    substate => substate.error || null,
  );

const makeSelectFeaturedPlaylistsItems = () =>
  createSelector(
    makeSelectFeaturedPlaylistsResource(),
    substate => get(substate, ['playlists', 'items'], []),
  );

const makeSelectFeaturedPlaylistsMessage = () =>
  createSelector(
    makeSelectFeaturedPlaylistsResource(),
    substate => substate.message || '',
  );

// Filters list selectors
const makeSelectFiltersList = () =>
  createSelector(
    selectFeaturedPlaylistsContainer,
    substate => substate.filtersList || {},
  );

const makeSelectFiltersListResource = () =>
  createSelector(
    makeSelectFiltersList(),
    substate => substate.resource || {},
  );

const makeSelectFiltersListIsLoading = () =>
  createSelector(
    makeSelectFiltersList(),
    substate => substate.isLoading || false,
  );

const makeSelectFiltersListError = () =>
  createSelector(
    makeSelectFiltersList(),
    substate => substate.error || null,
  );

const makeSelectFiltersListList = () =>
  createSelector(
    makeSelectFiltersListResource(),
    substate => substate.filters || [],
  );

export {
  selectFeaturedPlaylistsContainer,
  makeSelectFeaturedPlaylists,
  makeSelectFeaturedPlaylistsIsLoading,
  makeSelectFeaturedPlaylistsError,
  makeSelectFeaturedPlaylistsItems,
  makeSelectFeaturedPlaylistsMessage,
  makeSelectFiltersListList,
  makeSelectFiltersListIsLoading,
  makeSelectFiltersListError,
};
