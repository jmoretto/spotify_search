/* eslint-disable prettier/prettier */
/*
 *
 * FeaturedPlaylistsContainer constants
 *
 */

// Playlists actions
export const FETCH_PLAYLISTS = 'app/FeaturedPlaylistsContainer/FETCH_PLAYLISTS';
export const FETCH_PLAYLISTS_SUCCESS = 'app/FeaturedPlaylistsContainer/FETCH_PLAYLISTS_SUCCESS';
export const FETCH_PLAYLISTS_FAILURE = 'app/FeaturedPlaylistsContainer/FETCH_PLAYLISTS_FAILURE';
export const FETCH_PLAYLISTS_CANCEL = 'app/FeaturedPlaylistsContainer/FETCH_PLAYLISTS_CANCEL';

// Filters actions
export const FETCH_FILTERS = 'app/FeaturedPlaylistsContainer/FETCH_FILTERS';
export const FETCH_FILTERS_SUCCESS = 'app/FeaturedPlaylistsContainer/FETCH_FILTERS_SUCCESS';
export const FETCH_FILTERS_FAILURE = 'app/FeaturedPlaylistsContainer/FETCH_FILTERS_FAILURE';

// Token actions
export const VERIFY_TOKEN = 'app/FeaturedPlaylistsContainer/VERIFY_TOKEN';
