/*
 * HomePage
 *
 * This is the first thing users see of our App, at the '/' route
 *
 */

import React from 'react';
import { FormattedMessage } from 'react-intl';
import { SPOTIFY_AUTHORIZE_COMPLETE_API } from 'urls';
import messages from './messages';

function HomePage() {
  return (
    <div className="h-full">
      <a
        className="min-w-sm rounded-lg bg-green-900 border border-solid text-xl p-4 font-bold text-white hover:bg-green-500 focus:bg-green-200"
        href={SPOTIFY_AUTHORIZE_COMPLETE_API}
      >
        <FormattedMessage {...messages.authorizeWithSpotify} />
      </a>
    </div>
  );
}

export default HomePage;
