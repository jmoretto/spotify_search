/**
 *
 * App.js
 *
 * This component is the skeleton around the actual pages, and should only
 * contain code that should be seen on all pages. (e.g. navigation bar)
 *
 */

import React from 'react';
import { Switch, Route } from 'react-router-dom';

import FeaturedPlaylistsContainer from 'containers/FeaturedPlaylistsContainer';
import HomePage from 'containers/HomePage/Loadable';
import { FormattedMessage } from 'react-intl';
import messages from './messages';

export default function App() {
  return (
    <div className="h-full">
      <Header />
      <main className="h-full mx-auto container max-w-lptp w-full px-10 py-8">
        <Switch>
          <Route exact path="/" component={HomePage} />
          <Route
            exact
            path="/playlists"
            component={FeaturedPlaylistsContainer}
          />
          <Route
            component={() => (
              <h1>
                <FormattedMessage {...messages.notFound} />
              </h1>
            )}
          />
        </Switch>
      </main>
    </div>
  );
}

const Header = () => (
  <header className="h-40 w-full border-b border-sold">
    <div className="h-full flex items-center mx-auto pr-8 pl-10 container max-w-lptp">
      <h1>
        <FormattedMessage {...messages.spotIfood} />
      </h1>
    </div>
  </header>
);
