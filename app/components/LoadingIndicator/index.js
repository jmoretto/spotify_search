/**
 *
 * LoadingIndicator
 *
 */

import React, { memo } from 'react';
import localStyle from './styles.scss';

function LoadingIndicator() {
  return <div className={`relative ${localStyle.spinner}`} />;
}

export default memo(LoadingIndicator);
