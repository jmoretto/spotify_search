/* eslint-disable react/no-multi-comp */
/**
 *
 * Filter
 *
 */

import React, { memo } from 'react';
import PropTypes from 'prop-types';
import Select from 'react-select';
import DatePicker from 'react-datepicker';
import { INTEGER_TYPE, DATE_TIME_TYPE, DATE_FORMAT } from './constants';

import localStyle from './styles.scss';

function Filter(props) {
  const { filter } = props;
  // We need to parse the filter object to find out which type of
  // filter we should render. We will parse according to the
  // following rules:
  //  1. If we have a 'values' array then we render a select field.
  //  2. If we have a 'validation' object and entityType === DATE_TIME then we render a date/time picker.
  //  3. If we have a validation and primitive type === INTEGER we render an input of type number
  //  4. Else we just render a normal string text input.
  if (filter.values) {
    // Render a select field.
    return <SelectFilter {...props} />;
  }
  if (filter.validation && filter.validation.entityType === DATE_TIME_TYPE) {
    // Render a date/time picker.
    return <DateTimeFilter {...props} />;
  }

  return (
    <TextFilter
      {...props}
      isNumber={
        props.filter.validation &&
        props.filter.validation.primitiveType === INTEGER_TYPE
      }
    />
  );
}

// eslint-disable-next-line react/prop-types
const TextFilter = memo(({ filter, onChange, value, isNumber }) => (
  <label htmlFor={filter.id} className="min-w-50 px-6 mb-4">
    <div className="text-xl">{filter.name}</div>
    <input
      id={filter.id}
      name={filter.id}
      type={isNumber ? 'number' : 'text'}
      min={
        isNumber && filter.validation && filter.validation.min
          ? filter.validation.min
          : 0
      }
      max={
        isNumber && filter.validation && filter.validation.max
          ? filter.validation.max
          : undefined
      }
      onChange={evt => onChange(filter.id, evt.target.value)}
      value={value || ''}
      className="border border-solid border-gray-500 min-w-full h-10 outline-none text-xl px-2 rounded"
    />
  </label>
));

// eslint-disable-next-line react/prop-types
const SelectFilter = memo(({ filter, onChange }) => {
  const options = filter.values.reduce((acc, { value, name: label }) => {
    // eslint-disable-next-line no-param-reassign
    acc = [...acc];
    acc.push({ value, label });
    return acc;
  }, []);
  return (
    <label htmlFor={filter.id} className="min-w-50 px-6 mb-4">
      <div className="text-xl">{filter.name}</div>
      <Select
        onChange={value => {
          onChange(filter.id, value ? value.value : null);
        }}
        className={localStyle.select}
        options={options}
        isClearable
      />
    </label>
  );
});

// eslint-disable-next-line react/prop-types
const DateTimeFilter = memo(({ filter, onChange, value }) => (
  <label htmlFor={filter.id} className="min-w-50 px-6 mb-4">
    <div className="text-xl">{filter.name}</div>
    <DatePicker
      selected={value ? new Date(value) : null}
      onChange={dateValue =>
        onChange(
          filter.id,
          dateValue ? dateValue.toISOString(filter.validation.pattern) : null,
        )
      }
      showTimeSelect
      timeFormat="HH:mm"
      timeIntervals={15}
      timeCaption="Time"
      className="border border-solid border-gray-500 min-w-full h-10 outline-none text-xl px-2 rounded"
      placeholderText="Click to select a date and time"
      dateFormat={DATE_FORMAT}
    />
  </label>
));

Filter.propTypes = {
  filter: PropTypes.object.isRequired,
};

export default memo(Filter);
