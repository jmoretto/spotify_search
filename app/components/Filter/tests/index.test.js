/**
 *
 * Tests for Filter
 *
 * @see https://github.com/react-boilerplate/react-boilerplate/tree/master/docs/testing
 *
 */

import React from 'react';
import { render } from 'react-testing-library';
import { IntlProvider } from 'react-intl';
// import 'jest-dom/extend-expect'; // add some helpful assertions

import Filter from '../index';
import { DEFAULT_LOCALE } from '../../../i18n';

describe('<Filter />', () => {
  const props = {
    filter: {
      id: 'locale',
      name: 'Locale',
      values: [
        {
          value: 'en_AU',
          name: 'en_AU',
        },
        {
          value: 'de_DE',
          name: 'de_DE ',
        },
        {
          value: 'pt_BR',
          name: 'pt_BR',
        },
        {
          value: 'fr_FR',
          name: 'fr_FR',
        },
        {
          value: 'en_US',
          name: 'en_US',
        },
        {
          value: 'es_AR',
          name: 'es_AR',
        },
      ],
    },
  };

  it('Expect to not log errors in console', () => {
    const spy = jest.spyOn(global.console, 'error');
    render(
      <IntlProvider locale={DEFAULT_LOCALE}>
        <Filter {...props} />
      </IntlProvider>,
    );
    expect(spy).not.toHaveBeenCalled();
  });

  it('Should render and match the snapshot', () => {
    const {
      container: { firstChild },
    } = render(
      <IntlProvider locale={DEFAULT_LOCALE}>
        <Filter {...props} />
      </IntlProvider>,
    );
    expect(firstChild).toMatchSnapshot();
  });

  it('Should render a date picker and match the snapshot', () => {
    const datePickerProps = {
      filter: {
        id: 'timestamp',
        name: 'Data e Horário',
        validation: {
          primitiveType: 'STRING',
          entityType: 'DATE_TIME',
          pattern: 'yyyy-MM-ddTHH:mm:ss',
        },
      },
    };

    const {
      container: { firstChild },
    } = render(
      <IntlProvider locale={DEFAULT_LOCALE}>
        <Filter {...datePickerProps} />
      </IntlProvider>,
    );
    expect(firstChild).toMatchSnapshot();
  });

  it('Should render a text field and match the snapshot', () => {
    const textProps = {
      filter: {
        id: 'offset',
        name: 'Página',
        validation: {
          primitiveType: 'INTEGER',
        },
      },
    };

    const {
      container: { firstChild },
    } = render(
      <IntlProvider locale={DEFAULT_LOCALE}>
        <Filter {...textProps} />
      </IntlProvider>,
    );
    expect(firstChild).toMatchSnapshot();
  });
});
