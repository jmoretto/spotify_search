export const INTEGER_TYPE = 'INTEGER';
export const DATE_TIME_TYPE = 'DATE_TIME';
export const DATE_FORMAT = 'yyyy/MM/dd HH:mm:ss';
