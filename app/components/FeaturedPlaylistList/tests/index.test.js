/**
 *
 * Tests for FeaturedPlaylistList
 *
 * @see https://github.com/react-boilerplate/react-boilerplate/tree/master/docs/testing
 *
 */

import React from 'react';
import { render } from 'react-testing-library';
import { IntlProvider } from 'react-intl';
// import 'jest-dom/extend-expect'; // add some helpful assertions

import FeaturedPlaylistList from '../index';
import { DEFAULT_LOCALE } from '../../../i18n';

describe('<FeaturedPlaylistList />', () => {
  const props = {
    playlists: [
      {
        name: 'name',
        id: 'test',
        external_urls: {
          spotify: 'http://test.com',
        },
        images: [
          {
            url: 'http://image.url',
          },
        ],
      },
      {
        name: 'name2',
        id: 'test2',
        external_urls: {
          spotify: 'http://test2.com',
        },
        images: [
          {
            url: 'http://image2.url',
          },
        ],
      },
    ],
  };

  it('Expect to not log errors in console', () => {
    const spy = jest.spyOn(global.console, 'error');
    render(
      <IntlProvider locale={DEFAULT_LOCALE}>
        <FeaturedPlaylistList {...props} />
      </IntlProvider>,
    );
    expect(spy).not.toHaveBeenCalled();
  });

  it('Should render and match the snapshot', () => {
    const {
      container: { firstChild },
    } = render(
      <IntlProvider locale={DEFAULT_LOCALE}>
        <FeaturedPlaylistList {...props} />
      </IntlProvider>,
    );
    expect(firstChild).toMatchSnapshot();
  });
});
