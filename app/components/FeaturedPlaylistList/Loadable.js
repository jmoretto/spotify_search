/**
 *
 * Asynchronously loads the component for FeaturedPlaylistList
 *
 */

import loadable from 'utils/loadable';

export default loadable(() => import('./index'));
