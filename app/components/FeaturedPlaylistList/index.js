/**
 *
 * FeaturedPlaylistList
 *
 */

import React, { memo } from 'react';
import PropTypes from 'prop-types';

function FeaturedPlaylistList(props) {
  return (
    <div className="flex flex-wrap mb-4">
      {props.playlists.map(item => (
        <FeaturedPlaylistItem key={item.id} item={item} />
      ))}
    </div>
  );
}

// eslint-disable-next-line react/prop-types
const FeaturedPlaylistItem = ({ item }) => (
  <a
    href={item.external_urls.spotify}
    target="_blank"
    className="shadow flex flex-1 items-center min-w-42 md:min-w-20 sm:min-w-full max-w-2xl h-48 p-4 border-solid border mr-4 mb-4 last:mb-0 last:mr-0 rounded hover:border-gray-400 focus:bg-gray-100"
  >
    <div className="pr-4 mr-4 border-r border-solid">
      <img
        src={`${item.images[0].url}`}
        className="w-40 h-40 rounded-full"
        alt={`${item.name}`}
      />
    </div>
    <div className="text-2xl">{item.name}</div>
  </a>
);

FeaturedPlaylistList.propTypes = {
  playlists: PropTypes.array,
};

export default memo(FeaturedPlaylistList);
