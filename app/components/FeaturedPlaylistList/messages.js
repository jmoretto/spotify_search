/*
 * FeaturedPlaylistList Messages
 *
 * This contains all the text for the FeaturedPlaylistList component.
 */

import { defineMessages } from 'react-intl';

export const scope = 'app.components.FeaturedPlaylistList';

export default defineMessages({
  header: {
    id: `${scope}.header`,
    defaultMessage: 'This is the FeaturedPlaylistList component!',
  },
});
