/**
 *
 * Tests for FiltersList
 *
 * @see https://github.com/react-boilerplate/react-boilerplate/tree/master/docs/testing
 *
 */

import React from 'react';
import { render } from 'react-testing-library';
import { IntlProvider } from 'react-intl';
// import 'jest-dom/extend-expect'; // add some helpful assertions

import FiltersList from '../index';
import { DEFAULT_LOCALE } from '../../../i18n';

describe('<FiltersList />', () => {
  const props = {
    filters: [
      {
        id: 'locale',
        name: 'Locale',
        values: [
          {
            value: 'en_AU',
            name: 'en_AU',
          },
          {
            value: 'de_DE',
            name: 'de_DE ',
          },
          {
            value: 'pt_BR',
            name: 'pt_BR',
          },
          {
            value: 'fr_FR',
            name: 'fr_FR',
          },
          {
            value: 'en_US',
            name: 'en_US',
          },
          {
            value: 'es_AR',
            name: 'es_AR',
          },
        ],
      },
      {
        id: 'country',
        name: 'País',
        values: [
          {
            value: 'AU',
            name: 'Australia',
          },
          {
            value: 'DE',
            name: 'Alemanhã',
          },
          {
            value: 'BR',
            name: 'Brasil',
          },
          {
            value: 'PT',
            name: 'Portugal',
          },
          {
            value: 'en_US',
            name: 'EUA',
          },
          {
            value: 'RU',
            name: 'Rússia',
          },
        ],
      },
      {
        id: 'timestamp',
        name: 'Data e Horário',
        validation: {
          primitiveType: 'STRING',
          entityType: 'DATE_TIME',
          pattern: 'yyyy-MM-ddTHH:mm:ss',
        },
      },
      {
        id: 'limit',
        name: 'Quantidade',
        validation: {
          primitiveType: 'INTEGER',
          min: 1,
          max: 50,
        },
      },
      {
        id: 'offset',
        name: 'Página',
        validation: {
          primitiveType: 'INTEGER',
        },
      },
    ],
    onChange: () => {},
    values: {
      locale: 'en_US',
      country: 'AU',
      timestamp: '2019-02-02T11:11:11Z',
      limit: 1,
      offset: 1,
    },
  };

  it('Expect to not log errors in console', () => {
    const spy = jest.spyOn(global.console, 'error');
    render(
      <IntlProvider locale={DEFAULT_LOCALE}>
        <FiltersList {...props} />
      </IntlProvider>,
    );
    expect(spy).not.toHaveBeenCalled();
  });

  it('Should render and match the snapshot', () => {
    const {
      container: { firstChild },
    } = render(
      <IntlProvider locale={DEFAULT_LOCALE}>
        <FiltersList {...props} />
      </IntlProvider>,
    );
    expect(firstChild).toMatchSnapshot();
  });
});
