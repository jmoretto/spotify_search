/**
 *
 * FiltersList
 *
 */

import React, { memo } from 'react';
import PropTypes from 'prop-types';

import Filter from 'components/Filter';
import localStyle from './styles.scss';

const nameFilter = {
  id: 'name',
  name: 'Nome',
  validation: {
    primitiveType: 'STRING',
  },
};

export const FiltersList = ({
  filters,
  onChange,
  onChangeName,
  values,
  nameFilterValue,
}) => {
  const renderFilters = () =>
    filters.map(filterObj => (
      <Filter
        key={filterObj.id}
        filter={filterObj}
        onChange={onChange}
        value={values[filterObj.id]}
      />
    ));

  return (
    <div
      className={`shadow flex flex-wrap items-center min-w-40 md:min-w-20 sm:min-w-full p-4 border-solid border mb-4 rounded mr-16 ${
        localStyle.filterContainer
      }`}
    >
      {renderFilters()}
      <Filter
        key={nameFilter.id}
        filter={nameFilter}
        onChange={onChangeName}
        value={nameFilterValue}
      />
    </div>
  );
};

FiltersList.propTypes = {
  filters: PropTypes.array.isRequired,
  onChange: PropTypes.func,
  onChangeName: PropTypes.func,
  values: PropTypes.object,
  nameFilterValue: PropTypes.string,
};

export default memo(FiltersList);
