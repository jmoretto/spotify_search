/**
 *
 * Asynchronously loads the component for FiltersList
 *
 */

import loadable from 'utils/loadable';

export default loadable(() => import('./index'));
