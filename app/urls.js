/**
 * Contains all the URLs for this application.
 */

export const PLAYLISTS_PATH = '/playlists';

export const CLIENT_ID = '578e16f2f1de49cb83437c308a5960da';
export const LOCAL_HOST_URI = `${window.location.origin}${PLAYLISTS_PATH}`;

export const SPOTIFY_AUTHORIZE_API = 'https://accounts.spotify.com/authorize';
export const SPOTIFY_AUTHORIZE_COMPLETE_API = `${SPOTIFY_AUTHORIZE_API}?client_id=${CLIENT_ID}&response_type=token&redirect_uri=${LOCAL_HOST_URI}`;
export const SPOTIFY_FEATURED_PLAYLISTS_API =
  'https://api.spotify.com/v1/browse/featured-playlists';
export const FETCH_FILTERS_API =
  'https://www.mocky.io/v2/5a25fade2e0000213aa90776';
