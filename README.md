## How to run
1.  Run `npm install` to install necessary dependencies.
2.  Run `npm start` to start the app on port 3000 of localhost: `http://localhost:3000`.
3.  Run `npm start:production` to run the same app but in production mode (production code).

App is available for testing in:
https://evening-tor-26694.herokuapp.com

## Description of technologies/libraries used:

The objective of this app is to list playlists from the spotify API. The user can filter this list through the filter component.

This app was bootstrapped from react-boilerplate (https://github.com/react-boilerplate/react-boilerplate). Technologies and libs used:

 - React - View library (using hooks API)
 - Redux - State management
 - react-intl - i18n library
 - redux-sagas - Side-effect handling
 - TailwindCSS - Utility-first CSS library (also SASS and CSS Modules)
 - reselect - Library for memoization of state
 - Webpack - Bundling/process runner library (supports hot-reloading for better development experience)
 - react-router - Routing library
 - eslint - code linting
 - JEST - testing library (snapshot testing)

## Como rodar
1.  Rode `npm install` para instalar as dependências necessárias.
2.  Rode `npm start` para iniciar o aplicativo na porta 3000 do localhost: `http://localhost:3000`.
3.  Rode `npm start:production` para rodar o mesmo servidor só que com o código em modo de produção.

A aplicação está disponível pra testar em:
https://evening-tor-26694.herokuapp.com

## Descrição de tecnologias/libs usadass.

O aplicativo tem por objetivo listar playlists a partir da API do spotify. O usuário pode filtrar essa lista através do componente de filtros.

O aplicativo foi feito usando como base o react-boilerplate (https://github.com/react-boilerplate/react-boilerplate). As tecnologias/bibliotecas utilizadas foram:

 - React - View library (using hooks API)
 - Redux - State management
 - react-intl - i18n library
 - redux-sagas - Side-effect handling
 - TailwindCSS - Utility-first CSS library (also SASS and CSS Modules)
 - reselect - Library for memoization of state
 - Webpack - Bundling/process runner library (supports hot-reloading for better development experience)
 - react-router - Routing library
 - eslint - code linting
 - JEST - testing library (snapshot testing)
