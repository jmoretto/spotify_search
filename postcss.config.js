const tailwindcss = require('tailwindcss');
const autoprefixer = require('autoprefixer');
const postcssNested = require('postcss-nested');

module.exports = {
  ident: 'postcss',
  plugins: [tailwindcss('./tailwind.config.js'), autoprefixer, postcssNested],
};
